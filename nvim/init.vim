set number
set mouse=a
set numberwidth=1
syntax on
set showcmd
set ruler
set cursorline
set encoding=utf-8
set showmatch
set sw=2
set relativenumber
set encoding=UTF-8
so ~/.config/nvim/plugins.vim
so ~/.config/nvim/nerdtree_conf.vim
so ~/.config/nvim/maps.vim

