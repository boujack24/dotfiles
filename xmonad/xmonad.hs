import XMonad
import Data.Monoid
import System.Exit
import XMonad.Util.SpawnOnce
import XMonad.Util.Run
import XMonad.Layout.Spacing
import XMonad.Config.Desktop
import XMonad.Hooks.DynamicLog
import XMonad.Hooks.ManageDocks
import XMonad.Util.EZConfig(additionalKeys)
import XMonad.Hooks.ManageHelpers
import XMonad.Hooks.EwmhDesktops
import XMonad.Layout.Fullscreen
import System.IO
import XMonad.Layout.NoBorders
import XMonad.Actions.CycleWS
import Graphics.X11.ExtraTypes.XF86

import qualified XMonad.StackSet as W
import qualified Data.Map        as M



myTerminal      = "alacritty"

myFocusFollowsMouse :: Bool

myFocusFollowsMouse = True

myClickJustFocuses :: Bool

myClickJustFocuses = False

myBorderWidth   = 2

myModMask       = mod4Mask

myWorkspaces = ["\xf07c","\xf57d","\xf121","\xf518","\xf11b","\xf51f","\xf03d","\xf53f"]

xmobarEscape = concatMap doubleLts

  where doubleLts '<' = "<<"
        doubleLts x = [x]

myClickableWS :: [String]

myClickableWS = clickable . (map xmobarEscape) $ ["\xf07c","\xf57d","\xf121","\xf518","\xf11b","\xf51f","\xf03d","\xf53f"]

  where 
      clickable l = [ "<action=xdotool key super+" ++ show (n) ++ ">" ++ ws ++ "</action>" | (i,ws) <- zip [1..8] l, let n = i ]

myNormalBorderColor  = "#000000"

myFocusedBorderColor = "#7137c8"

myKeys conf@(XConfig {XMonad.modMask = modm}) = M.fromList $

    -- launch a terminal
    [ ((modm .|. shiftMask, xK_Return), spawn $ XMonad.terminal conf)

    -- launch dmenu
    , ((modm,               xK_p     ), spawn "dmenu_run")


    , ((modm .|. shiftMask, xK_p     ), spawn "gmrun")

    -- close focused window
    , ((modm .|. shiftMask, xK_c     ), kill)

     -- Rotate through the available layout algorithms
    , ((modm,               xK_space ), sendMessage NextLayout)

    --  Reset the layouts on the current workspace to default
    , ((modm .|. shiftMask, xK_space ), setLayout $ XMonad.layoutHook conf)

    -- Resize viewed windows to the correct size
    , ((modm,               xK_n     ), refresh)

    -- Move focus to the next window
    , ((modm,               xK_Tab   ), windows W.focusDown)

    -- Move focus to the next window
    , ((modm,               xK_j     ), windows W.focusDown)

    -- Move focus to the previous window
    , ((modm,               xK_k     ), windows W.focusUp  )

    -- Move focus to the master window
--    , ((modm,               xK_m     ), windows W.focusMaster  )

    -- Swap the focused window and the master window
    , ((modm,               xK_Return), windows W.swapMaster)

    -- Swap the focused window with the next window
    , ((modm .|. shiftMask, xK_j     ), windows W.swapDown  )

    -- Swap the focused window with the previous window
    , ((modm .|. shiftMask, xK_k     ), windows W.swapUp    )

    -- Shrink the master area
    , ((modm,               xK_h     ), sendMessage Shrink)

    -- Expand the master area
    , ((modm,               xK_l     ), sendMessage Expand)

    -- Push window back into tiling
    , ((modm,               xK_t     ), withFocused $ windows . W.sink)

--CUSTOM
   -- , ((modm,               xK_b     ), spawn "brave")
    , ((modm,               xK_b     ), spawn "com.brave.Browser")
    , ((modm,               xK_f     ), spawn "pcmanfm ~/")
    , ((modm,               xK_r     ), spawn "rofi -show combi")
    , ((modm,               xK_s     ), spawn "pavucontrol")
    , ((modm,               xK_m     ), spawn "spotify")
    , ((modm,               xK_g     ), spawn "steam")
    , ((modm .|. shiftMask, xK_s     ), spawn "flameshot launcher")
    -- Increment the number of windows in the master area
    , ((modm              , xK_comma ), sendMessage (IncMasterN 1))

    -- Deincrement the number of windows in the master area
    , ((modm              , xK_period), sendMessage (IncMasterN (-1)))

    -- Toggle the status bar gap
    -- Use this binding with avoidStruts from Hooks.ManageDocks.
    -- See also the statusBar function from Hooks.DynamicLog.
    --
    -- , ((modm              , xK_b     ), sendMessage ToggleStruts)
    , ((0,xF86XK_AudioLowerVolume), spawn "pactl set-sink-volume @DEFAULT_SINK@  -5%")
	, ((0,xF86XK_AudioRaiseVolume), spawn "pactl set-sink-volume @DEFAULT_SINK@  +5%")
	, ((0,xF86XK_AudioMute), spawn "pactl set-sink-mute @DEFAULT_SINK@ toggle")
    -- Quit xmonad
    , ((modm .|. shiftMask, xK_q     ), io (exitWith ExitSuccess))

    -- Restart xmonad
    , ((modm              , xK_q     ), spawn "xmonad --recompile; xmonad --restart")

    --move to next-prev workspace
    , ((modm,               xK_Right),  nextWS)
    , ((modm,               xK_Left),    prevWS)
    , ((modm .|. shiftMask, xK_Right),  shiftToNext)
    , ((modm .|. shiftMask, xK_Left),    shiftToPrev)
    -- Run xmessage with a summary of the default keybindings (useful for beginners)
    , ((modm .|. shiftMask, xK_slash ), spawn ("echo \"" ++ help ++ "\" | xmessage -file -"))
    ]
    ++

    --
    -- mod-[1..9], Switch to workspace N
    -- mod-shift-[1..9], Move client to workspace N
    --
    [((m .|. modm, k), windows $ f i)
        | (i, k) <- zip (XMonad.workspaces conf) [xK_1 .. xK_9]
        , (f, m) <- [(W.greedyView, 0), (W.shift, shiftMask)]]
    -- ++
    
    --
    -- mod-{w,e,r}, Switch to physical/Xinerama screens 1, 2, or 3
    -- mod-shift-{w,e,r}, Move client to screen 1, 2, or 3
    --
    --[((m .|. modm, key), screenWorkspace sc >>= flip whenJust (windows . f))
      --  | (key, sc) <- zip [xK_w, xK_e, xK_r] [0..]
       -- , (f, m) <- [(W.view, 0), (W.shift, shiftMask)]]


------------------------------------------------------------------------
-- Mouse bindings: default actions bound to mouse events
--
myMouseBindings (XConfig {XMonad.modMask = modm}) = M.fromList $

    -- mod-button1, Set the window to floating mode and move by dragging
    [ ((modm, button1), (\w -> focus w >> mouseMoveWindow w
                                       >> windows W.shiftMaster))

    -- mod-button2, Raise the window to the top of the stack
    , ((modm, button2), (\w -> focus w >> windows W.shiftMaster))

    -- mod-button3, Set the window to floating mode and resize by dragging
    , ((modm, button3), (\w -> focus w >> mouseResizeWindow w
                                       >> windows W.shiftMaster))

    -- you may also bind events to the mouse scroll wheel (button4 and button5)
    ]


myLayout = avoidStruts (tiled ||| Mirror tiled ||| Full)
  
  where
     -- default tiling algorithm partitions the screen into two panes
     tiled   = Tall nmaster delta ratio
     -- The default number of windows in the master pane
     nmaster = 1
     -- Default proportion of screen occupied by master pane
     ratio   = 1/2
     -- Percent of screen to increment by when resizing panes
     delta   = 3/100

myManageHook = composeAll
    [ className =? "MPlayer"        --> doFloat
    , className =? "Gimp"           --> doFloat
    , resource  =? "desktop_window" --> doIgnore
    , resource  =? "kdesktop"       --> doIgnore
    , className =? "rdesktop"       --> doFloat 
    , className =? "Zenity"       --> doFloat 
    , className =? "Brave-browser"  --> doShift (myClickableWS !! 1)
    , className =? "Chromium"       --> doShift (myClickableWS !! 1)
    , className =? "Pavucontrol"    --> doShift (myClickableWS !! 5)
    , className =? "Spotify"        --> doShift (myClickableWS !! 5)
    , className =? "code-oss"       --> doShift (myClickableWS !! 2)
    , className =? "Gimp"           --> doShift (myClickableWS !! 7)
    , className =? "kdenlive"       --> doShift (myClickableWS !! 6)
    , className =? "Steam"          --> doShift (myClickableWS !! 4)
    , className =? "Pcmanfm"         --> doShift (myClickableWS !! 0)
    , className =? "Xfce-polkit"    --> doFloat
    , className =? "DesktopEditors" --> doShift (myClickableWS !! 3)
    , className =? "okular"         --> doShift (myClickableWS !! 3)
    , className =? "akregator"      --> doShift (myClickableWS !! 1)
    ]

myEventHook = mempty

myLogHook = return ()

myStartupHook = do 
                  spawnOnce "nitrogen --restore &"
                  spawnOnce "/usr/lib/xfce-polkit/xfce-polkit &"
		  spawnOnce "setxkbmap es"
                  spawnOnce "powerkit"
		  spawnOnce "~/.xmonad/./java_apps.sh"
		  spawnOnce "export QT_QPA_PLATFORMTHEME=qt5ct &"
		  spawnOnce "dunst &"
        --spawnOnce "xmobar -x 0 &"
	--spawnOnce "tint2 &"

main = do
         xmproc <- spawnPipe "xmobar /home/bou/.xmobarrc"
         --xmproc <- spawnPipe "picom --experimental-backends -b"
         --xmproc <- spawnPipe "tint2 -c /home/bou/.config/tint2/tint2rc"
         --xmproc <- spawnPipe "tint2 -c /home/bou/.config/tint2/tint2rcApps"
         --xmonad $  docks defaults
	 xmonad $ fullscreenSupport $ docks $ ewmh defaults
                 {
                  manageHook = manageDocks <+> myManageHook 
                                           <+> (isFullscreen --> doFullFloat)
                                           <+> manageHook desktopConfig
                 , layoutHook = smartBorders $ avoidStruts $ spacingWithEdge 2 $  layoutHook desktopConfig  -- spacingWithEdge 10
                 , workspaces = myClickableWS
		 , handleEventHook = docksEventHook <+> handleEventHook defaults
		 , startupHook = docksStartupHook <+> startupHook defaults
                 , logHook = dynamicLogWithPP xmobarPP
                        { ppOutput = hPutStrLn xmproc
                        , ppCurrent = xmobarColor "#ffd5f6""" . wrap "<fn=0><box type=Bottom width=2 mb=2 color=#ffd5f6>" "</box></fn>"
			, ppHidden = wrap "<fn=0>" "</fn>"
                        , ppHiddenNoWindows = xmobarColor "#892ca0" "" . wrap "<fn=0>" "</fn>"
                        , ppTitle   = xmobarColor "#ab54ff"  "" . shorten 40
                        , ppVisible = wrap "(" ")"
                        , ppUrgent  = xmobarColor "red" "yellow"
                        
}
}

defaults = def {
      -- simple stuff
        terminal           = myTerminal,
        focusFollowsMouse  = myFocusFollowsMouse,
        clickJustFocuses   = myClickJustFocuses,
        borderWidth        = myBorderWidth,
        modMask            = myModMask,
        workspaces         = myClickableWS,
        normalBorderColor  = myNormalBorderColor,
        focusedBorderColor = myFocusedBorderColor,
      -- key bindings
        keys               = myKeys,
        mouseBindings      = myMouseBindings,
      -- hooks, layouts
        layoutHook         = myLayout,
        manageHook         = myManageHook,
        handleEventHook    = myEventHook,
        logHook            = myLogHook,
        startupHook        = myStartupHook
    }

help :: String
help = unlines ["The default modifier key is 'alt'. Default keybindings:",
    "",
    "-- launching and killing programs",
    "mod-Shift-Enter  Launch xterminal",
    "mod-p            Launch dmenu",
    "mod-Shift-p      Launch gmrun",
    "mod-Shift-c      Close/kill the focused window",
    "mod-Space        Rotate through the available layout algorithms",
    "mod-Shift-Space  Reset the layouts on the current workSpace to default",
    "mod-n            Resize/refresh viewed windows to the correct size",
    "",
    "-- move focus up or down the window stack",
    "mod-Tab        Move focus to the next window",
    "mod-Shift-Tab  Move focus to the previous window",
    "mod-j          Move focus to the next window",
    "mod-k          Move focus to the previous window",
    "mod-m          Move focus to the master window",
    "",
    "-- modifying the window order",
    "mod-Shift-j  Swap the focused window with the next window",
    "mod-Shift-k  Swap the focused window with the previous window",
    "",
    "-- resizing the master/slave ratio",
    "mod-h  Shrink the master area",
    "mod-l  Expand the master area",
    "",
    "-- floating layer support",
    "mod-t  Push window back into tiling; unfloat and re-tile it",
    "",
    "-- increase or decrease number of windows in the master area",
    "mod-comma  (mod-,)   Increment the number of windows in the master area",
    "mod-period (mod-.)   Deincrement the number of windows in the master area",
    "",
    "-- quit, or restart",
    "mod-Shift-q  Quit xmonad",
    "mod-q        Restart xmonad",
    "mod-[1..9]   Switch to workSpace N",
    "",
    "-- Workspaces & screens",
    "mod-Shift-[1..9]   Move client to workspace N",
    "mod-{w,e,r}        Switch to physical/Xinerama screens 1, 2, or 3",
    "mod-Shift-{w,e,r}  Move client to screen 1, 2, or 3",
    "",
    "-- Mouse bindings: default actions bound to mouse events",
    "mod-button1  Set the window to floating mode and move by dragging",
    "mod-button2  Raise the window to the top of the stack",
    "mod-button3  Set the window to floating mode and resize by dragging"]
