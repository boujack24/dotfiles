#!/bin/bash
mkdir -p ~/dotfiles/vim
mkdir -p ~/dotfiles/xmonad
mkdir -p ~/dotfiles/nvim
mkdir -p ~/dotfiles/xmobar
cp ~/.xmonad/xmonad.hs ~/dotfiles/xmonad/
cp ~/.xmobarrc ~/dotfiles/xmobar/
cp ~/.vim/vimrc ~/dotfiles/vim/
cp -R ~/.config/alacritty ~/dotfiles/
cp ~/.config/nvim/init.vim ~/dotfiles/nvim/
cp -R ~/.config/tint2 ~/dotfiles/
cp -R ~/.config/fish ~/dotfiles/
cp -R ~/.config/omf ~/dotfiles/
read -p "Comentario de commit: " COMENT
cd ~/dotfiles
git add . 
git commit -m "$COMENT"
git push
echo "presione una tecla para salir"
read -n 1
exit
